terraform {

    backend "http" {
    address = "https://gitlab.com/api/v4/projects/28818789/terraform/state/azuread-state"
    lock_address ="https://gitlab.com/api/v4/projects/28818789/terraform/state/azuread-state/lock"
    unlock_address ="https://gitlab.com/api/v4/projects/28818789/terraform/state/azuread-state/lock"     
    lock_method ="POST"
    unlock_method ="DELETE"
    retry_wait_min = "5"
    }
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.12.0"
    }
  }

}
#azure ad provider
provider "azuread" {
  
    client_id       = var.ARM_CLIENT_ID
    client_secret   = var.ARM_CLIENT_SECRET
    tenant_id       = var.TENANT_ID
}

module "azure-ad" {
    source         =  "./modules/azure-ad"  
    password       =  var.USER_PASSWORD  
}