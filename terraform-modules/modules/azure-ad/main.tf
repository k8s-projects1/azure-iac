data "azuread_client_config" "current" {}

resource "azuread_user" "example" {
  user_principal_name = "tindo@tendaimz.onmicrosoft.com"
  display_name        = "Tindo"
  mail_nickname       = "tindo"
  password            =  var.password
}

resource "azuread_group" "example" {
  display_name     = "example"
  owners           = [data.azuread_client_config.current.object_id]
  security_enabled = true
}

data "azuread_user" "tindo" {
  user_principal_name = "tindo@tendaimz.onmicrosoft.com"  
}
data "azuread_group" "example" {
  display_name     = "example"
  security_enabled = true
}
resource "azuread_group_member" "example" {
  group_object_id  = azuread_group.example.id
  member_object_id = data.azuread_user.tindo.id
}