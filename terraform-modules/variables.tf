 variable "ARM_SUBSCRIPTION_ID" {
  description = "Azure subscription id"
  type        = string  
}

variable "ARM_CLIENT_ID" {
  description = "Azure client id"
  type        = string  
}

variable "TENANT_ID" {
  description = "Azure ad tenant"
  type        = string   
}

variable "ARM_CLIENT_SECRET" {
  description = "Service principal password"
  type        = string 
}

variable "USER_PASSWORD" {
  description = "User account password"
  type        = string 
}
