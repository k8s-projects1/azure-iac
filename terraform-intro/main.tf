terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/28818789/terraform/state/az-state"
    lock_address ="https://gitlab.com/api/v4/projects/28818789/terraform/state/az-state/lock"
    unlock_address ="https://gitlab.com/api/v4/projects/28818789/terraform/state/az-state/lock"     
    lock_method ="POST"
    unlock_method ="DELETE"
    retry_wait_min = "5"
    }
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }

  
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}

  subscription_id = var.ARM_SUBSCRIPTION_ID
  client_id       = var.ARM_CLIENT_ID
  client_secret   = var.ARM_CLIENT_SECRET
  tenant_id       = var.ARM_TENANT_ID
}

resource "azurerm_resource_group" "rg" {
  name     = "myTerraformRG"
  location = "westus2"
}
