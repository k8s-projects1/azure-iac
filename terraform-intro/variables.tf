 variable "ARM_SUBSCRIPTION_ID" {
  description = "Azure subscription id"
  type        = string  
}

variable "ARM_CLIENT_ID" {
  description = "Azure client id"
  type        = string  
}

variable "ARM_TENANT_ID" {
  description = "Azure tenant"
  type        = string   
}

variable "ARM_CLIENT_SECRET" {
  description = "Service principal password"
  type        = string 
}

# variable "USERNAME" {
#   description = "gitlab backend user name"
#   type        = string   
# }


# variable "TOKEN" {
#   description = "gitlab backend password"
#   type        = string   
# }
